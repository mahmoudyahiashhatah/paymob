package com.task.paymob.NetWork;


import com.task.paymob.Models.FeatureResponse;


import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Field;
import retrofit2.http.Part;
import retrofit2.http.Query;


public interface ServiceApi {

    @Multipart
    @POST("NewTxn/ServiceSelector")
    Call<FeatureResponse> getFeature(
            @Part("TYPE") RequestBody type,
            @Part("LOGIN") RequestBody login,
            @Part("PASSWORD") RequestBody password,
            @Part("REQUEST_GATEWAY_CODE") RequestBody rqGetWayCode,
            @Part("REQUEST_GATEWAY_TYPE") RequestBody rqGetWayType

            );


}

