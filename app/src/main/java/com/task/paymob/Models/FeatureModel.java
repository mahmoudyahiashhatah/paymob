package com.task.paymob.Models;

import java.io.Serializable;

public class FeatureModel implements Serializable {



    private String merchant_image;
    private String merchant_name;
    private String mpg_macro_merchant_id;


    public String getMerchant_image() {
        return merchant_image;
    }

    public String getMerchant_name() {
        return merchant_name;
    }


    public String getMpg_macro_merchant_id() {
        return mpg_macro_merchant_id;
    }


}
