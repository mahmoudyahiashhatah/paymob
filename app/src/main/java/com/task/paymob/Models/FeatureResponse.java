package com.task.paymob.Models;

import java.io.Serializable;
import java.util.ArrayList;

public class FeatureResponse implements Serializable {


        private String TXNSTATUS;
        private ArrayList<FeatureModel> ARRAY;

    public String getTXNSTATUS() {
        return TXNSTATUS;
    }

    public void setTXNSTATUS(String TXNSTATUS) {
        this.TXNSTATUS = TXNSTATUS;
    }

    public ArrayList<FeatureModel> getARRAY() {
        return ARRAY;
    }

    public void setARRAY(ArrayList<FeatureModel> ARRAY) {
        this.ARRAY = ARRAY;
    }
}
