package com.task.paymob.listners;

import android.view.View;


public interface OnItemClickListener {

     void onItemClick(View view, String code);
}
