package com.task.paymob.UI.Fragments;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;
import com.pnikosis.materialishprogress.ProgressWheel;
import com.task.paymob.Models.FeatureModel;
import com.task.paymob.Models.FeatureResponse;
import com.task.paymob.NetWork.RetroWeb;
import com.task.paymob.NetWork.ServiceApi;
import com.task.paymob.R;
import com.task.paymob.UI.Activity.ScannerActivity;
import com.task.paymob.UI.Activity.SecondPageActivity;
import com.task.paymob.UI.Adapters.FeatureAdapter;
import com.task.paymob.listners.OnItemClickListener;

import java.util.ArrayList;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.pedant.SweetAlert.SweetAlertDialog;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class StoreFragment extends Fragment implements OnItemClickListener {


    private static final int BARCODE_READER_REQUEST_CODE = 1;
    FeatureAdapter featureAdapter;

    ArrayList<FeatureModel> featureModels;

    LinearLayoutManager lm;


    @BindView(R.id.rvFeature)
    RecyclerView rvFeature;

    @BindView(R.id.etCode)
    EditText etCode;

    @BindView(R.id.ivQr)
    ImageView ivQr;


    @BindView(R.id.progressWheelLoad)
    ProgressBar progressWheel;




    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_store, container, false);

        ButterKnife.bind(this, view);

        featureModels = new ArrayList<>();



        featureAdapter = new FeatureAdapter(getContext(), featureModels, this);

        lm = new LinearLayoutManager(getContext(), RecyclerView.HORIZONTAL, false);

        rvFeature.setLayoutManager(lm);
        rvFeature.setAdapter(featureAdapter);
        getLisFeature();


        return view;
    }

    @Override
    public void onItemClick(View view, String code) {
        etCode.setText(code);

    }

    private void getLisFeature() {
        progressWheel.setVisibility(View.VISIBLE);

        String Type = "FEATMERREQ";

        String App = "App";

        RequestBody body = RequestBody.create(MediaType.parse("text/plain"), Type);

        RequestBody app = RequestBody.create(MediaType.parse("text/plain"), App);


        RetroWeb.getClient()
                .create(ServiceApi.class)
                .getFeature(body, app, app, app, app)
                .enqueue(new Callback<FeatureResponse>() {
                    @Override
                    public void onResponse(Call<FeatureResponse> call, Response<FeatureResponse> response) {
                        progressWheel.setVisibility(View.GONE);
                        rvFeature.setVisibility(View.VISIBLE);
                        if (response.isSuccessful())
                            if (response.body().getTXNSTATUS().equals("200")) {
                                if (response.body().getARRAY() != null) {

                                    featureModels.clear();
                                    featureModels.addAll(response.body().getARRAY());
                                    featureAdapter.notifyDataSetChanged();
                                }

                            }

                    }

                    @Override
                    public void onFailure(Call<FeatureResponse> call, Throwable t) {
                        progressWheel.setVisibility(View.GONE);
                        rvFeature.setVisibility(View.VISIBLE);

                    }
                });
    }


    @OnClick(R.id.btn_add)
    void btnAddCode() {
        if (etCode.getText().toString().equals("")) {
            allertDialog(false, etCode.getText().toString());
        } else {
            allertDialog(true, etCode.getText().toString());
        }
    }

    private void allertDialog(boolean isHaveCode, String code) {
        if (isHaveCode) {

            new SweetAlertDialog(Objects.requireNonNull(getContext()), SweetAlertDialog.SUCCESS_TYPE)
                    .setTitleText("Success")
                    .setContentText("#" + code)
                    .show();
        } else {
            new SweetAlertDialog(getActivity())
                    .setTitleText("Oops...")
                    .setContentText("you should enter code!")
                    .show();
        }

    }


    @OnClick(R.id.ivQr)
    void scanner() {

        if (ActivityCompat
                .checkSelfPermission(Objects.requireNonNull(getContext()), Manifest.permission.CAMERA)
                != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{Manifest.permission.CAMERA},
                    0);
        } else {
             IntentIntegrator.forSupportFragment(this).setCaptureActivity(ScannerActivity.class).initiateScan();
           // IntentIntegrator.fo(this).initiateScan();
        }
    }

    @OnClick(R.id.tvSecondActivity)
    void tvSecondActivity(){
            Intent intent=new Intent(getContext(), SecondPageActivity.class);
            startActivity(intent);

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
        //check for null
        if (result != null) {
            if (result.getContents() == null) {
                Toast.makeText(getContext(), "Scan Cancelled", Toast.LENGTH_LONG).show();
            } else {
                etCode.setText(result.getContents());
            }
        }
    }
}


