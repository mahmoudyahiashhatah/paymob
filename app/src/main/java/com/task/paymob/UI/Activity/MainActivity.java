package com.task.paymob.UI.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.task.paymob.R;
import com.task.paymob.UI.Fragments.OnlineFragment;
import com.task.paymob.UI.Fragments.ServicesFragment;
import com.task.paymob.UI.Fragments.StoreFragment;

import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {




    public static FragmentManager mFragmentManager;
    private FragmentTransaction mTransaction;





    StoreFragment storeFragment = new StoreFragment();
    OnlineFragment onlineFragment = new OnlineFragment();
    ServicesFragment servicesFragment = new ServicesFragment();

    boolean flagIsStore;

    private TextView[] btn = new TextView[3];
    private TextView btn_unfocus;
    private int[] btn_id = {R.id.tv_store, R.id.tv_service,R.id.tv_online};
    int foucase;





    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        mFragmentManager=getSupportFragmentManager();

        for(int i = 0; i < btn.length; i++){
            btn[i] = findViewById(btn_id[i]);
            btn[i].setOnClickListener(this);
        }


        btn_unfocus = btn[0];
        foucase=0;

        btn_unfocus.setBackground(getDrawable(R.color.colorWhite));
        btn_unfocus.setTextColor(getResources().getColor(R.color.colorPrimary));


        loadFragment(storeFragment);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){

            case R.id.tv_store :
                loadFragment(storeFragment);
                flagIsStore=true;

                setFocus(btn_unfocus, btn[0],0);
                break;
            case R.id.tv_service :
                loadFragment(servicesFragment);
                flagIsStore=false;
                setFocus(btn_unfocus, btn[1],1);
                break;
            case R.id.tv_online :
                loadFragment(onlineFragment);
                flagIsStore=false;
                setFocus(btn_unfocus, btn[2],2);
                break;

        }

    }



    private void setFocus(TextView btn_unfocused, TextView btn_focus,int current ){

        btn_unfocused.setBackground(getDrawable(R.color.colorPrimary));
        btn_unfocused.setTextColor(getResources().getColor(R.color.colorWhite));

        btn_focus.setBackground(getDrawable(R.color.colorWhite));
        btn_focus.setTextColor(getResources().getColor(R.color.colorPrimary));
        this.btn_unfocus = btn_focus;
        foucase=current;
    }




    private void loadFragment(Fragment fragment) {


        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.addToBackStack(null);
        ft.replace(R.id.frame_content, fragment);
        ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        ft.commit();



    }

    @Override
    public void onBackPressed() {

        if (flagIsStore)
        {
            finish();
        }else {
            setFocus(btn_unfocus, btn[0],0);
            // Fragment  fragment = new HomeFragment();
            loadFragment(storeFragment);

            flagIsStore=true;
        }


    }
}
