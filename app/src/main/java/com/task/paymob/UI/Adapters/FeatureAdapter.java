package com.task.paymob.UI.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.squareup.picasso.Picasso;
import com.task.paymob.Models.FeatureModel;
import com.task.paymob.R;
import com.task.paymob.listners.OnItemClickListener;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

public class FeatureAdapter extends  RecyclerView.Adapter<FeatureAdapter.MyViewHolder> {


    private ArrayList<FeatureModel> featureList;
    private OnItemClickListener onItemClickListener;
    private Context context;

    public FeatureAdapter(Context context, ArrayList<FeatureModel> featureList, OnItemClickListener onItemClickListener) {
        this.featureList = featureList;
        this.onItemClickListener=onItemClickListener;
        this.context=context;
    }

    @NonNull
    @Override
    public FeatureAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.card_feture, parent, false);

        return new MyViewHolder(itemView);
    }



    @Override
    public void onBindViewHolder(@NonNull FeatureAdapter.MyViewHolder holder, int position) {
        FeatureModel feature=featureList.get(position);
        holder.itemView.setOnClickListener(view -> onItemClickListener.onItemClick(view,feature.getMpg_macro_merchant_id()));
        holder.tvFeature.setText(feature.getMerchant_name());
        holder.tvCode.setText(feature.getMpg_macro_merchant_id());

        Glide
                .with(context)
                .load("http://52.28.140.193:8080"+feature.getMerchant_image())
                .placeholder(R.mipmap.user_profile)
                .into(holder.ivFeature);



//        Picasso.get().load(feature.getMerchant_image()).into(holder.ivFeature);

    }

    @Override
    public int getItemCount() {
        return featureList.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.ivFeature)
        CircleImageView ivFeature;

        @BindView(R.id.tvFeature)
        TextView tvFeature;

        @BindView(R.id.tvCode)
        TextView tvCode;


        MyViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
