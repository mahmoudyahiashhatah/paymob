package com.task.paymob.UI.Activity;

import android.os.Bundle;
import android.util.DisplayMetrics;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import com.task.paymob.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SecondPageActivity extends AppCompatActivity {


    @BindView(R.id.tvBalance)
    TextView tvBalance;

    @BindView(R.id.laCredit)
    CardView laCredit;

    @BindView(R.id.laCreditBack)
    CardView laCreditBack;

    @BindView(R.id.tvMaster)
    TextView tvMaster;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second_page);
        ButterKnife.bind(this);


        StringBuilder s=new StringBuilder( "25466542125436");

        tvBalance.setText("934.00");
        laCredit.setLayoutParams(setParm());
        laCreditBack.setLayoutParams(setParm());
        for (int i=4;i<s.length();i+=5){
            s.insert(i," ");
        }

        tvMaster.setText(s);

    }

    private LinearLayout.LayoutParams setParm(){
        DisplayMetrics displayMetrics = getResources().getDisplayMetrics();
        int width = displayMetrics.widthPixels;

        int height = displayMetrics.heightPixels;

        if ((height*0.5)>(width*0.5)){
            height=(int)(width*0.7);
        }else {
            height= (int) (height*0.5);
        }
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                (int)(width*0.95),height
        );

        params.setMargins(20,10,20,10);




        return  params;
    }
}
